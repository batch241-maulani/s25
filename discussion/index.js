// JSON Objects


//JSON
/*
{
	"city": "Quezon City",
	"province": "Metro Manila",
	"country": "Philippines"



}*/



//JSON, Array
/*"cities": [
	{"city": "Quezon City", "province": "Metro Manila", "country": "Philippines"}
	{"city": "Manila City", "province": "Metro Manila", "country": "Philippines"}
	{"city": "Makati City", "province": "Metro Manila", "country": "Philippines"}
]*/


//PARSE: Converting JSON string into objects
let batchesJSON = `[{"batchName":"Batch X"},{"batchName": "Batch Y"}]`

console.log(JSON.parse(batchesJSON))


let stringifiedObject = `{"name":"Eric","age":"9","address":{"city":"Bonifacio Global City","country":"Philippines"}}`
console.log(JSON.parse(stringifiedObject))


// STRINGIFY: Convert Objects into String(JSON)
// JSON.stringify
let data = {
	name: "Gabryl",
	age: 61,
	address: {
		city: "New York",
		country: "USA"
	}
}

console.log(data)

let stringData = JSON.stringify(data)
console.log(stringData)


